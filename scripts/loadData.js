import {
    getData
} from './getData.js';

const wishList = ['idd001', 'idd003', 'idd005', 'idd007'];
const cartList = [{
        id: 'idd015',
        count: 3,
    },
    {
        id: 'idd025',
        count: 1,
    },
    {
        id: 'idd035',
        count: 2,
    },
    {
        id: 'idd045',
        count: 3,
    },
];

export const loadData = () => {
    if (location.search) {
        const search = decodeURI(location.search);
        const prop = search.split('=')[0].substring(1);
        const value = search.split('=')[1]; // use "let" if try filter more than 1 word

        if (prop === 's') {
            getData.search(value, data => console.log(data));
        } else if (prop === 'wishlist') {
            getData.wishList(wishList, data => console.dir({
                wishlist: data
            }));
        } else if (prop === 'cat' || prop === 'subcat') {
            getData.category(prop, value, data => console.log(data));
        }
        // if (value.includes("+")) {
        //     value = value.split("+").join(" ");
        // };
    }

    if (location.hash) {
        getData.item(location.hash.substring(1), data => console.log(data));
    }

    if (location.pathname.includes('cart')) {
        getData.cart(cartList, data => console.log(data));
    }

    // getData.catalog(data => console.log(data));
    // getData.subCatalog("Декор", data => console.log(data));
};